# OpenML dataset: pair0005

https://www.openml.org/d/43299

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

//Add the description.md of the data file pair0005

Cause-effect is a growing database with two-variable cause-effect pairs 
created at Max-Planck-Institute for Biological Cybernetics in Tuebingen, Germany.
==================================================================================================================================================

Some pairs are highdimensional, for machine readability the relevant information about this is coded in Meta-data.

Meta-data contains the following information:

number of pair | 1st column of cause | last column of cause | 1st column of effect | last column of effect | dataset weight

The dataset weight should be used for calculating average performance of causal inference methods
to avoid a bias introduced by having multiple copies of essentially the same data (for example,
the pairs 56-63).

When you use this data set in a publication, please cite the following paper (which
also contains much more detailed information regarding this data set in the supplement):

J. M. Mooij, J. Peters, D. Janzing, J. Zscheischler, B. Schoelkopf
"Distinguishing cause from effect using observational data: methods and benchmarks"
Journal of Machine Learning Research 17(32):1-102, 2016

NOTE: pair0001 - pair0041 are taken from the UCI Machine Learning Repository:

Asuncion, A. & Newman, D.J. (2007). UCI Machine Learning Repository [http://www.ics.uci.edu/~mlearn/MLRepository.html]. Irvine, CA: University of California, School of Information and Computer Science. 

==================================================================================================================================================
Overview over all data pairs.

			var 1				var 2					dataset			ground truth

pair0001		Altitude			Temperature				DWD			->
pair0002		Altitude			Precipitation				DWD			->
pair0003		Longitude			Temperature				DWD			->
pair0004		Altitude			Sunshine hours				DWD			->

Information for pairs0005:

https://archive.ics.uci.edu/ml/datasets/Abalone

1. Title of Database: Abalone data

2. Sources:

   (a) Original owners of database:
	Marine Resources Division
	Marine Research Laboratories - Taroona
	Department of Primary Industry and Fisheries, Tasmania
	GPO Box 619F, Hobart, Tasmania 7001, Australia
	(contact: Warwick Nash +61 02 277277, wnash@dpi.tas.gov.au)

   (b) Donor of database:
	Sam Waugh (Sam.Waugh@cs.utas.edu.au)
	Department of Computer Science, University of Tasmania
	GPO Box 252C, Hobart, Tasmania 7001, Australia

   (c) Date received: December 1995

3. Attribute information:

   Given is the attribute name, attribute type, the measurement unit and a
   brief description.  

	Name		Data Type	Meas.	Description
	----		---------	-----	-----------
x:	Rings		integer			+1.5 gives the age in years
y:	Length		continuous	mm	Longest shell measurement


Ground truth:
x --> y

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43299) of an [OpenML dataset](https://www.openml.org/d/43299). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43299/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43299/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43299/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

